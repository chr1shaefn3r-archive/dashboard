FROM ubuntu:trusty

ENV DEBIAN_FRONTEND noninteractive

# Add compiler package and ruby1.9.1
RUN apt-get update; apt-get install -y build-essential ruby1.9.1-dev nodejs

# Install dashing and bundle
RUN gem install dashing; gem install bundle

WORKDIR /app
ADD . /app
RUN bundle

EXPOSE 3030/tcp
CMD []
ENTRYPOINT ["dashing", "start"]

